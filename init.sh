#!/bin/bash

# Ask the user for their name
echo - - - - - - - - - - - - - - - - - -
echo --- Digitalu - strapi starter -----
echo - - - - - - - - - - - - - - - - - -
echo " "
echo " "
echo " "

read -p 'Enter the url: ' url


cat > nginx.conf <<EOF
events {

}

http {

  ssl_session_cache   shared:SSL:10m;
  ssl_session_timeout 10m;

  server {
    server_name $url;

    listen 80;
    listen 443 ssl;

    keepalive_timeout   70;

    ssl_certificate /etc/letsencrypt/live/$url/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/$url/privkey.pem;

    location / {
      proxy_pass http://strapi:1337;
    }

        location /test {
      proxy_pass http://strapi:1337;
    }

    location /33 {
      return 200;
    }
  }
}
EOF

echo "### UPDATING REPOS & INSTALLING EVERYTHING"
apt-get update 
apt-get install git docker docker-compose certbot

echo "### GETTING SSL CERTIFICATE"
certbot certonly --standalone -d $url -m developer@digitalu.be --no-eff-email --agree-tos

echo "### STARTING DOCKER"
docker-compose pull 
docker-compose up




# certbot renew --pre-hook "docker-compose -f path/to/docker-compose.yaml down" --post-hook "docker-compose -f path/to/docker-compose.yaml up -d"
